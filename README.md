# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|N|

## Website Detail Description
基本操作：
 * 上下左右控制飛行船方向
 * 空白鍵發射子彈
 * z 使用技能 
 * x c v 對應 自動瞄準、補血、特殊彈藥(雙倍傷害)
 * p 暫停遊戲
 * q 重新啟動遊戲
 * home 靜音切換、page up調高音量、page down調低音量
 * 右下角有角色血條，怪物血量可以依據顏色由綠到紅為
 * 75%up(綠) ~  50%up(黃) ~ 25%up(橙) ~ 25%low(紅)
# Basic Components Description : 
1. Jucify mechanisms : 
    * level : 我的level作法是設定兩個global 變數
    分別是 difficulty 與 level，然後生成怪物是依據
    level的高低，來取得不同的怪物外觀，並且在每關結束後增加level，並在製造怪物時，依照level做出不同層數怪物，而攻擊強度、怪物血量、發射速度皆會依照difficulty 與 level去調控。

    * skill : 玩家按下Z時，將子彈射速條快很多，然後讓子彈的velocity.x random取 -100 ~ 100，做出類似散彈機關槍效果，當然這個技能可以兼容其他隨附技能。
2. Animations :
    * player的animations主要是讓飛船後方推進器會噴火，然後在前後左右移動時會改變火的大小

3. Particle Systems : 
    * 主要為explosion funciton處理，裡面會依照傳入的16進位數字調整顏色，也依此我讀入的pixel變換顏色。
4. Sound effects : 
    * 分成登入背景音樂、普通戰鬥音樂、魔王背景音樂。
    * 發射時會有音效。
    * 在技能冷卻結束後會撥放"叮"一聲。
5. Leaderboard : 只有儲存當下最高分，以及上一次的分數。

# Bonus Functions Description : 
* 以下的觸發機制皆為在角色碰到補給包後，才能使用。
1.  Bullet automatic aiming : Auto / useAuto / closeAuto 三個function來控制效果持續時間及可操作時機，值得一提的是在
    update裡面需要去呼叫每一個bullet(因為是group)的rotation，轉向至target，然後還有call moveToXY讓子彈追蹤敵人，
    判定方式為取出第一個偵測到為活著的敵人。
    
2. Unique bullet : SPBullet / useSPBullet / closeSPBullet 三個function控制效果持續時間與操作時機。主要功能只是加強子彈傷害，以及使用另外的
    子彈圖片，並且子彈會有隨機延遲效果。
    
3.  HP healer : Heal / useHeal / closeHeal 三個function控制效果出現與操作時機，只要按下該指令即可補滿血，即血條補滿。

4. Boss : 在通過三關之後，就會出現boss。boss有不同的背景音樂、不同的射擊方式，不同的移動方式。

5. 一開始選單處可以按0看操作說明，遊戲名稱上方可以調整遊戲難度 (關卡數不變，但是會增強敵人的子彈飛行速度、血量)。
