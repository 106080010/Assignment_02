var game = new Phaser.Game(800, 600, Phaser.AUTO, 'canvas');

// the least difficulty = 4
game.global = { score:0, best:0, level:1, win:false, lose:false, SP:false,
                COOL:false, stageChange:false, cur_time:0, pause:false, hit:1,
                difficulty: 4, T:0, auto:false, heal:false, spB:false, movement: 0};

game.state.add('boot', bootState);
game.state.add('load', loadState);
game.state.add('menu', menuState);
game.state.add('play', playState);

game.state.start('boot');