var loadState = {
    preload: function(){
        var loadingLabel = game.add.text(game.width/2, game.height/2, 'loading...', {font:'30px Arial', fill:'#ffffff'});
        loadingLabel.anchor.setTo(0.5,0.5);

        var progressBar = game.add.sprite(game.width/2, game.height/2 + 50, 'progressBar');
        progressBar.anchor.setTo(0.5,0.5);
        game.load.setPreloadSprite(progressBar);

        //Load all game assets
        game.load.spritesheet('player', 'src/SpaceShip.png', 59, 63);
        game.load.spritesheet('invader1', 'src/enemy.png', 90, 60);
        game.load.spritesheet('invader2', 'src/enemy2.png', 90, 60);
        game.load.spritesheet('invader3', 'src/enemy3.png', 90, 60);
        game.load.spritesheet('Boss', 'src/boss.png', 123, 67);
        game.load.image('background', 'src/space.jpg');
        game.load.image('button', 'src/button.png');
        game.load.audio('bgMusic', 'src/FadeX.mp3');
        game.load.audio('strMusic', 'src/start.mp3');
        game.load.audio('shooting', 'src/shooting_se.mp3');
        game.load.audio('bossMusic', 'src/boss.mp3');
        game.load.audio('ready', 'src/ready.mp3');
        game.load.image('bullet', 'src/bullet.png');
        game.load.image('plus', 'src/plus.png');
        game.load.image('minus', 'src/minus.png');
        game.load.image('bullet_sp', 'src/bullet_sp.png');
        game.load.image('enemyBullet', 'src/enemy_bullet.png');
        game.load.image('pixel', 'src/pixel.png');
        game.load.image('skill', 'src/skill.png');
        game.load.image('skill_ac', 'src/skill_active.png');
        game.load.image('spBullet', 'src/specialbullet.png');
        game.load.image('spBullet_s', 'src/specialbullet_s.png');
        game.load.image('heal', 'src/heal.png');
        game.load.image('heal_s', 'src/heal_s.png');
        game.load.image('autoAim', 'src/autoAim.png');
        game.load.image('autoAim_s', 'src/autoAim_s.png');
    },
    create: function () {
        game.state.start('menu');
    }
};