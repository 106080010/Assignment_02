var music;
var square;
var rule;
var button;
var diff;
var menuState = {
    create: function () {
        game.add.image(0, 0, 'background');

        diff = game.add.text(game.width/2, game.height/2 -180, 'difficulty : ' + game.global.difficulty, {font: '26px Arial', fill:'#ffffff'});
        diff.anchor.setTo(0.5, 0.5);
        var plus = game.add.button(game.width/2 + 96, game.height/2 -180, 'plus', this.UP, this);
        plus.anchor.setTo(0.5, 0.5);
        var minus = game.add.button(game.width/2 - 96, game.height/2 -180, 'minus', this.DOWN, this);
        minus.anchor.setTo(0.5, 0.5);

        var nameLabel = game.add.text(game.width/2, game.height/2 - 90, 'War of Spacecraft', {font: '60px Arial', fill:'#ffffff'});
        nameLabel.anchor.setTo(0.5, 0.5);

        var scoreLabel = game.add.text(game.width/2, game.height/2, 'Last score : ' + game.global.score, {font:'25px Arial', fill:"#ffffff"});
        scoreLabel.anchor.setTo(0.5, 0.5);
        var scoreBest = game.add.text(game.width/2, game.height/2 + 40, 'Best recent score : ' + game.global.best, {font:'25px Arial', fill:"#ffffff"});
        scoreBest.anchor.setTo(0.5, 0.5);

        var startLabel = game.add.text(game.width/2, game.height/2 + 140, 'press enter to start', {font:"25px Arial", fill:"#ffffff"});
        startLabel.anchor.setTo(0.5, 0.5);

        var howto = game.add.text(game.width/2, game.height/2 + 180, 'How to play (press 0)', {font:"25px Arial", fill:"#ffffff"});
        howto.anchor.setTo(0.5, 0.5);

        startLabel.alpha = 0;
        howto.alpha = 0;

        var tween = game.add.tween(startLabel).to( { alpha: 1 }, 2000, "Linear", true, 0, -1);
        tween.yoyo(true, 2000);
        var tween2 = game.add.tween(howto).to( { alpha: 1 }, 2000, "Linear", true, 0, -1);
        tween2.yoyo(true, 2000);

        var spKey = game.input.keyboard.addKey(Phaser.Keyboard.ENTER);
        spKey.onDown.add(this.start, this);
        var ht = game.input.keyboard.addKey(Phaser.Keyboard.ZERO);
        ht.onDown.add(this.HowTo, this);

        var bmd = this.drawRect(game.width*2/3, game.height*2/3, '#000000');
        square = game.add.sprite(game.world.centerX, game.world.centerY, bmd);
        square.anchor.setTo(0.5, 0.5);
        square.alpha = 0;

        rule = game.add.text(game.world.centerX, game.world.centerY, ' Z : use skill \n' +
                                                                            ' X : auto aiming (need item)\n' +
                                                                            ' C : heal to full HP (need item)\n' +
                                                                            ' V : Special bullets (double damage, need item)\n' +
                                                                            ' P : pause game\n' +
                                                                            ' Q : reboot game (in game)\n' +
                                                                            ' Space : launch fire\n' +
                                                                            ' up / down / right / left : control spacecraft\n\n' +
                                                                            ' *Items will occur after passing any stage*', {font:"25px Arial", fill:"#ffffff"});
        rule.anchor.setTo(0.5, 0.5);
        rule.alpha = 0;

        button = game.add.button(game.world.centerX + game.width/3, game.world.centerY - game.height/3, 'button', this.close, this);
        button.anchor.setTo(0.5, 0.5);
        button.scale.setTo(0.05, 0.05);
        button.alpha = 0;

        music = game.add.audio('strMusic');
        music.volume = 0.8;
        music.loopFull();
    },
    start: function () {
        music.destroy();
        game.state.start('play');
    },
    HowTo: function () {
        square.alpha = 1;
        rule.alpha = 1;
        button.alpha = 1;
    },
    close: function(){
        square.alpha = 0;
        rule.alpha = 0;
        button.alpha = 0;
    },
    UP: function(){
        game.global.difficulty += (game.global.difficulty < 12)? 1 : 0;
        diff.text = 'difficulty : ' + game.global.difficulty;
    },
    DOWN: function(){
        game.global.difficulty -= (game.global.difficulty > 4)? 1 : 0;
        diff.text = 'difficulty : ' + game.global.difficulty;
    },
    drawRect: function(w, h, color){
        var bmd = game.add.bitmapData(w, h);
        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, w, h);
        bmd.ctx.fillStyle = color;
        bmd.ctx.fill();
        return bmd;
    }
};