var player;
var aliens;
var bullets;
var shooting;
var skill_ready;
var skill;
var autoAim, autoAim_s;
var heal, heal_s;
var spBullet, spBullet_s;
var skill_ac;
var bulletTime = 0;
var lives;
var enemyBullets;
var emitter;
var fireButton;
var pauseButton;
var superWeapon;
var firingTimer = 0;
var stateText;
var scoreString;
var stageString;
var scoreText;
var livingEnemies = [];
var bossMusic;
var square;
var time_t;
var timer;
var ready;
var tilemap;
const frameW = 90, frameH = 60;
const bossW = 123, bossH = 67;
const skillH = 86 ;
const helperH = 32 ;
const damage = 15; // enemy hit player
const maxHP = 10; // real max hp = damage * maxHP
const hPI = Math.PI/2;
const fps = 0.016;
const topLevel = 4;
var cursors;
var music;
var musicBoss;
var bullet;
var playState = {
    preload: function () {
    },
    create: function () {
        game.global.movement = 0;
        game.global.score = 0;
        game.global.level = 1;
        game.global.hit = 1;
        //game.global.difficulty = 4;
        game.global.T = 0;
        game.global.win = false;
        game.global.lose = false;
        game.global.SP = false;
        game.global.spB = false;
        game.global.heal = false;
        game.global.auto = false;
        game.global.COOL = false;
        game.global.stageChange = false;
        game.global.pause = false;
        game.global.cur_time = game.time.now;
        time_t = game.global.cur_time;

        // map
        tilemap = game.add.tileSprite(0, 0,800,600, 'background');

        //  Our bullet group
        this.resetBullet('bullet');

        // The enemy's bullets
        enemyBullets = game.add.group();
        enemyBullets.enableBody = true;
        enemyBullets.physicsBodyType = Phaser.Physics.ARCADE;
        enemyBullets.createMultiple(30, 'enemyBullet');
        enemyBullets.setAll('anchor.x', 0.5);
        enemyBullets.setAll('anchor.y', 1);
        enemyBullets.setAll('outOfBoundsKill', true);
        enemyBullets.setAll('checkWorldBounds', true);

        // player
        player = game.add.sprite(game.world.width/2, game.world.height - 60, 'player');
        player.anchor.setTo(0.5,0.5);
        player.health = damage*maxHP;
        player.maxHealth = damage*maxHP;
        player.animations.add('right', [3,4,5], 8, true);
        player.animations.add('left', [3,4,5], 8, true);
        player.animations.add('forward', [3,4,5], 8, true);
        player.animations.add('backward', [3,4,5], 8, true);
        player.animations.add('fly', [1,2], 8, true);

        //  The enemies!
        aliens = game.add.group();
        aliens.enableBody = true;
        aliens.physicsBodyType = Phaser.Physics.ARCADE;

        this.createAliens();

        //score
        scoreString = 'Score : ';
        scoreText = game.add.text(10, 10, scoreString + game.global.score, { font: '34px Arial', fill: '#fff' });

        //  Text
        stateText = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '84px Arial', fill: '#eff4ff' });
        stateText.anchor.setTo(0.5, 0.5);
        stateText.visible = false;

        stageString = game.add.text(game.world.centerX,game.world.centerY,' ', { font: '64px Arial', fill: '#eff4ff' });
        stageString.anchor.setTo(0.5, 0.5);
        stageString.alpha = 1;
        stageString.vibrate = false;

        //  Lives
        var bglive = this.drawRect(damage*maxHP + 10,(damage/2)*3 + 6,'#2f2f2f');
        var live = this.drawRect(damage*maxHP,(damage/2)*3, '#ff000c');
        var bglives = game.add.sprite(game.width - (damage*3-5), game.height - (damage*2.5 + 3), bglive );
        bglives.alpha = 0.9;
        lives = game.add.sprite(game.width - (damage*3), game.height - damage*2.5, live);
        bglives.anchor.setTo(1,0);
        lives.anchor.setTo(1,0);
        var hp = game.add.text(game.width - (damage*3 - 5 + damage*maxHP + 10), game.height - (damage*2.5 + 3), 'HP : ', { font: '24px Arial', fill: '#fff' });
        hp.anchor.setTo(1,0);

        //particle
        emitter = game.add.emitter(0, 0, 500);
        emitter.makeParticles('pixel');
        emitter.setYSpeed(-150, 150);
        emitter.setXSpeed(-150, 150);
        emitter.setScale(2, 0, 2, 0, 800);
        emitter.gravity = 0;
        emitter.forEach(function(particle)
        {
            particle.tint = 0x6770FF;
        });

        //skill
        skill = game.add.sprite(0,game.height - skillH,"skill");
        skill_ac = game.add.sprite(0,game.height - skillH,"skill_ac");
        skill_ac.alpha = 0;

        //little helper
        autoAim = game.add.sprite(0,game.height - skillH*2, 'autoAim');
        heal = game.add.sprite(0,game.height - skillH*3, 'heal');
        spBullet = game.add.sprite(0,game.height - skillH*4, 'spBullet');
        autoAim.alpha = 0;
        heal.alpha = 0;
        spBullet.alpha = 0;

        autoAim_s = game.add.sprite(game.world.centerX - helperH*2, game.world.centerY, 'autoAim_s');
        heal_s = game.add.sprite(game.world.centerX, game.world.centerY, 'heal_s');
        spBullet_s = game.add.sprite(game.world.centerX + helperH*2, game.world.centerY, 'spBullet_s');
        autoAim_s.anchor.setTo(0.5, 0.5);
        heal_s.anchor.setTo(0.5, 0.5);
        spBullet_s.anchor.setTo(0.5, 0.5);
        autoAim_s.alpha = 0;
        heal_s.alpha = 0;
        spBullet_s.alpha = 0;


        //square for skill cool down
        var bmd = this.drawRect(skillH, skillH, '#000000');
        square = game.add.sprite(0, game.height, bmd);
        square.alpha = 0.8;

        //music
        music = game.add.audio('bgMusic');
        shooting = game.add.audio('shooting');
        skill_ready = game.add.audio('ready');
        musicBoss = game.add.audio('bossMusic');
        musicBoss.volume = 1;
        shooting.volume = 0.6;
        music.volume = 0.6;
        music.loopFull();
        var upkey = game.input.keyboard.addKey(Phaser.Keyboard.PAGE_UP);
        var downkey = game.input.keyboard.addKey(Phaser.Keyboard.PAGE_DOWN);
        var mutekey = game.input.keyboard.addKey(Phaser.Keyboard.HOME);
        var quit = game.input.keyboard.addKey(Phaser.Keyboard.Q);
        var a = game.input.keyboard.addKey(Phaser.Keyboard.X);
        var h = game.input.keyboard.addKey(Phaser.Keyboard.C);
        var s = game.input.keyboard.addKey(Phaser.Keyboard.V);

        //keyboard event
        cursors = game.input.keyboard.createCursorKeys();
        upkey.onDown.add(this.volumeUp, this);
        downkey.onDown.add(this.volumeDown, this);
        mutekey.onDown.add(this.mute, this);
        quit.onDown.add(this.GameReset, this);
        a.onDown.add(this.useAuto, this);
        h.onDown.add(this.useHeal, this);
        s.onDown.add(this.useSPBullet, this);
        fireButton = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR);
        superWeapon = game.input.keyboard.addKey(Phaser.Keyboard.Z);
        pauseButton = game.input.keyboard.addKey(Phaser.Keyboard.P);
        pauseButton.onDown.add(this.GamePause, this);

        //physics enable
        game.physics.arcade.enable(player);
        game.physics.arcade.enable(autoAim_s);
        game.physics.arcade.enable(heal_s);
        game.physics.arcade.enable(spBullet_s);
        player.body.collideWorldBounds = true;
    },
    update: function () {
        tilemap.tilePosition.y += 2;

        if(!game.global.pause) {
            if (game.global.win) {
                game.time.events.add(Phaser.Timer.SECOND * 4, this.GameRestart, this);
                stateText.text = " You Win \n Wait " + (game.time.events.duration / 1001 + 1).toString()[0] + " Seconds";
                stateText.visible = true;
            } else if (game.global.lose) {
                game.time.events.add(Phaser.Timer.SECOND * 4, this.GameRestart, this);
                stateText.text = " You Lose \n Wait " + (game.time.events.duration / 1001 + 1).toString()[0] + " Seconds";
                stateText.visible = true;
            } else if (player.alive && !game.global.stageChange) {
                this.movePlayer();

                if(game.global.level == topLevel) {
                    this.moveBoss();
                } else if(game.global.level < topLevel) {
                    this.moveAliens();
                }

                if (fireButton.isDown) {
                    this.fireBullet();
                }

                if(game.global.auto) {
                    bullets.forEachAlive(function (bullet) {

                        // put every living enemy in an array
                        bullet.body.velocity.y = 0;
                        var alien = aliens.getFirstAlive();
                        if (alien)
                            bullet.rotation = -game.physics.arcade.angleBetween(bullet, alien) + 180;
                        game.physics.arcade.moveToXY(bullet, alien.body.x, alien.body.y, 300);
                    });
                }

                if (superWeapon.isDown && !game.global.COOL) {
                    skill_ac.alpha = 1;
                    game.time.events.add(Phaser.Timer.SECOND * 6, this.coolDown, this);
                    ready = game.time.create(false);
                    ready.add(Phaser.Timer.SECOND * 45, this.weaponReady, this);
                    timer = game.time.create(false);
                    timer.repeat(Phaser.Timer.SECOND, 45, this.SkillCool, this);
                    game.global.SP = true;
                }


                if (game.time.now > firingTimer) {
                    this.enemyFires();
                }

                //  Run collision
                game.physics.arcade.overlap(bullets, aliens, this.collisionHandler, null, this);
                game.physics.arcade.overlap(enemyBullets, player, this.enemyHitsPlayer, null, this);
                if(autoAim_s.alpha == 1) game.physics.arcade.overlap(autoAim_s, player, this.Auto, null, this);
                if(heal_s.alpha == 1) game.physics.arcade.overlap(heal_s, player, this.Heal, null, this);
                if(spBullet_s.alpha == 1) game.physics.arcade.overlap(spBullet_s, player, this.SPBullet, null, this);
            }
        }
    },
    GamePause: function(){
        game.global.pause = !game.global.pause;
        if(game.global.pause) {
            stateText.text = " Game Pause ";
            player.body.velocity.x = 0;
            player.body.velocity.y = 0;
            time_t = game.time.now;
            game.paused = true;
            stateText.visible = true;
        } else {
            stateText.visible = false;
            game.global.cur_time += game.time.now - time_t;
            game.paused = false;
        }
    },
    GameReset: function(){
        if(musicBoss) musicBoss.destroy();
        if(music) music.destroy();
        player.destroy();
        bullets.destroy();
        enemyBullets.destroy();
        aliens.destroy();
        spBullet_s.destroy();
        autoAim_s.destroy();
        heal_s.destroy();
        game.state.start('boot');
    },
    GameRestart: function() {
        game.global.best = (game.global.best < game.global.score)? game.global.score : game.global.best;
        if(musicBoss) musicBoss.destroy();
        if(music) music.destroy();
        player.destroy();
        bullets.destroy();
        enemyBullets.destroy();
        aliens.destroy();
        game.state.start('menu');
    },
    ChangeStage: async function(){
        stageString.visible = false;
        stageString.alpha = 1;
        game.global.level += 1;
        if(game.global.level == 2){
            spBullet_s.alpha = 1;
        } else if(game.global.level == 3){
            autoAim_s.alpha = 1;
        }
        else if(game.global.level == topLevel) {
            heal_s.alpha = 1;
            spBullet_s.alpha = 1;
            music.destroy();
            music = musicBoss;
            music.loopFull();
        }
        await this.createAliens();
        game.global.stageChange = false;
    },
    createAliens: function(){
        if(game.global.level == topLevel){
            var boss = aliens.create(bossW + 100, bossH + 10, 'Boss');
            boss.health = (game.global.level + game.global.difficulty) * 20;
            boss.maxHealth = (game.global.level + game.global.difficulty) * 20;
            boss.tint = 0x00FF10;
            boss.anchor.setTo(0.5, 0.5);
            boss.animations.add('move', [0, 1], 4, true);
            boss.play('move');
            boss.body.velocity.x = 100;
            //boss.body.moves = false;
            aliens.x = 0;
            aliens.y = game.world.height/8;
        } else {
            for (var y = 0; y < game.global.level; y++) {
                for (var x = 0; x < 5; x++) {
                    var alien = aliens.create(x * frameW, y * frameH, ('invader' + game.global.level));
                    alien.health = (game.global.level + game.global.difficulty + 1) * 2;
                    alien.maxHealth = (game.global.level + game.global.difficulty + 1) * 2;
                    alien.tint = 0x00FF10;
                    alien.anchor.setTo(0.5, 0.5);
                    alien.animations.add('move', [0, 1], 4, true);
                    alien.play('move');
                    alien.body.moves = false;
                }
            }
            aliens.x = game.world.width/8;
            aliens.y = game.world.height/8;
        }

    },
    movePlayer: function() {
        if (cursors.left.isDown) {
            player.body.velocity.x = -100;
            player.animations.play('left');

        } else if(cursors.right.isDown){
            player.body.velocity.x = 100;
            player.animations.play('right');

        } else if(cursors.up.isDown){
            player.body.velocity.y = -100;
            player.animations.play('forward');

        } else if(cursors.down.isDown){
            player.body.velocity.y = 100;
            player.animations.play('backward');

        } else {
            player.body.velocity.x = 0;
            player.body.velocity.y = 0;
            player.animations.play('fly');
        }
    },
    moveAliens: function(){
        if(aliens.x >  game.world.width/8 + 200)
            game.global.movement = -100 * fps;
        else if(aliens.x <= game.world.width/8)
            game.global.movement = 100 * fps;

        aliens.x = aliens.x + game.global.movement;

    },
    moveBoss: function(){
        aliens.forEachAlive(function(BOSS) {
            if(BOSS.x > 7*game.world.width/8)
                BOSS.body.velocity.x = -100;
            else if(BOSS.x < game.world.width/8)
                BOSS.body.velocity.x = 100;
            game.global.T += fps;
            BOSS.body.velocity.y = 100*hPI*Math.cos(hPI * game.global.T);
        }, this);
    },
    collisionHandler: function  (bullet, alien) {

        this.explosion(bullet, 0xA0AEFF);
        //  When a bullet hits an alien we kill them both
        alien.health -= game.global.hit;
        if(alien.health <= 0) {
            alien.kill();
            if(game.global.level == topLevel)
                game.global.score += 500*(game.global.difficulty + game.global.level);
            else
                game.global.score += 50*(game.global.difficulty + game.global.level - 4);
            scoreText.text = scoreString + game.global.score;
        }

        if(alien.health >= alien.maxHealth*0.75){
            alien.tint = 0x00FF10;
        }else if(alien.health >= alien.maxHealth*0.5){
            alien.tint = 0xFFF700;
        }else if(alien.health >= alien.maxHealth*0.25){
            alien.tint = 0xFFA400;
        }else {
            alien.tint = 0xFF0D00;
        }

        //  Increase the score

        //  And create an explosion :)

        if (aliens.countLiving() == 0)
        {
            if(game.global.level == topLevel){
                game.global.win = true;
            } else {
                enemyBullets.callAll('kill');
                game.global.stageChange = true;
                game.global.score += 500*(game.global.level + 1 + game.global.difficulty - 4);
                scoreText.text = scoreString + game.global.score;
                player.x = game.world.width / 2;
                player.y = game.world.height - 60;
                player.body.velocity.x = 0;
                player.body.velocity.y = 0;
                stageString.visible = true;
                stageString.text = 'Stage ' + game.global.level + ' Clear';
                var tween1 = game.add.tween(stageString).to({alpha: 0}, 2000, Phaser.Easing.Linear.None);
                tween1.start();
                game.time.events.add(Phaser.Timer.SECOND * 3, this.ChangeStage, this);
            }
        }
    },
    enemyHitsPlayer: function (player,bullet) {

        this.explosion(bullet, 0xFF6655);

        player.health -= damage;
        lives.width -= damage;

        // When the player dies
        if (player.health <= 0)
        {
            player.kill();
            enemyBullets.callAll('kill');
            game.global.lose = true;
        }

    },
    enemyFires: function () {

        //  Grab the first bullet we can from the pool
        var enemyBullet = enemyBullets.getFirstExists(false);

        livingEnemies.length=0;

        aliens.forEachAlive(function(alien){

            // put every living enemy in an array
            livingEnemies.push(alien);
        });

        if (enemyBullet && livingEnemies.length > 0)

        {

            var random = game.rnd.integerInRange(0,livingEnemies.length-1);

            // randomly select one of them
            var shooter = livingEnemies[random];
            // And fire the bullet from this enemy
            enemyBullet.reset(shooter.body.x, shooter.body.y);
            if(game.global.level == topLevel){
                var randomP = game.rnd.integerInRange(100, 200);
                game.physics.arcade.moveToObject(enemyBullet, player, 100 + randomP);
            }else {
                game.physics.arcade.moveToObject(enemyBullet, player, 100 + (game.global.level + game.global.difficulty - 4) * 25);
            }
            firingTimer = game.time.now + 2000/game.global.level;
        }

    },
    fireBullet: function () {

        //  To avoid them being allowed to fire too fast we set a time limit
        if (game.time.now > bulletTime)
        {
            //  Grab the first bullet we can from the pool
            bullet = bullets.getFirstExists(false);

            if (bullet)
            {
                //  And fire it
                bullet.reset(player.x, player.y - 30);
                bullet.body.velocity.y = -400;
                if(game.global.SP && !game.global.COOL) {
                    bullet.body.velocity.x = game.rnd.integerInRange(-100, 100);
                    bulletTime = game.time.now + 50;
                } else {
                    if(game.global.spB) {
                        var randomB = game.rnd.integerInRange( 100, 350);
                        bulletTime = game.time.now + randomB;
                    } else {
                        bulletTime = game.time.now + 200;
                    }
                }
                shooting.play();
            }
        }

    },
    coolDown : function(){
        skill_ac.alpha = 0;
        square.position.y = game.height - skillH;
        ready.start();
        timer.start();
        game.global.COOL = true;
    },
    SkillCool: function(){
        square.position.y += (skillH/45);
    },
    Heal: function(){
        heal.alpha = 1;
        heal_s.alpha = 0;
    },
    useHeal: function(){
        if(heal.alpha == 1) {
            heal.alpha = 0;
            this.closeHeal();
        }
    },
    closeHeal: function(){
        player.health = player.maxHealth;
        lives.width = player.maxHealth;
    },
    Auto: function(){
        autoAim.alpha = 1;
        autoAim_s.alpha = 0;
    },
    useAuto: function(){
        if(autoAim.alpha == 1) {
            autoAim.alpha = 0;
            game.global.auto = true;
            game.time.events.add(Phaser.Timer.SECOND * 6, this.closeAuto, this);
        }
    },
    closeAuto: function(){
        game.global.auto = false;
    },
    SPBullet: function(){
        spBullet.alpha = 1;
        spBullet_s.alpha = 0;
    },
    useSPBullet: async function(){
        if(spBullet.alpha == 1) {
            spBullet.alpha = 0;
            game.global.hit = 2;
            game.global.spB = true;
            await this.resetBullet('bullet_sp');
            game.time.events.add(Phaser.Timer.SECOND * 6, this.closeSPBullet, this);
        }
    },
    closeSPBullet : async function(){
        game.global.hit = 1;
        game.global.spB = false;
        await this.resetBullet('bullet');
    },
    weaponReady:function(){
        game.global.COOL = false;
        game.global.SP = false;
        skill_ready.play();
    },
    explosion: function(target, hexColor){
        emitter.x = target.x;
        emitter.y = target.y;
        target.kill();
        emitter.forEach(function(particle)
        {
            particle.tint = hexColor;
        });
        emitter.start(true, 800, null, 15);
    },
    resetBullet: function (bulletT) {

        if(bullets) bullets.destroy();
        bullets = game.add.group();
        bullets.enableBody = true;
        bullets.physicsBodyType = Phaser.Physics.ARCADE;
        bullets.createMultiple(30, bulletT);
        bullets.setAll('anchor.x', 0.5);
        bullets.setAll('anchor.y', 1);
        bullets.setAll('outOfBoundsKill', true);
        bullets.setAll('checkWorldBounds', true);

    },
    drawRect: function(w, h, color){
        var bmd = game.add.bitmapData(w, h);
        bmd.ctx.beginPath();
        bmd.ctx.rect(0, 0, w, h);
        bmd.ctx.fillStyle = color;
        bmd.ctx.fill();
        return bmd;
    },
    volumeUp: function () {
        music.volume += (music.volume < 2.0)? 0.1 : 0;
    },
    volumeDown: function () {
        music.volume -= (music.volume > 0.1)? 0.1 : 0;
    },
    mute: function () {
        music.mute = !music.mute;
    },
    render:function () {
        game.debug.text("Time : " + (game.time.now - game.global.cur_time) / 1000, game.width - 128, 32);
        game.debug.text("difficulty : " + game.global.difficulty, game.width - 146, 50);
        game.debug.text("Volume : " + ((music.mute)? "mute" : music.volume.toFixed(1)), game.width - 128, 68);
    }
};